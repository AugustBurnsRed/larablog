<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    /**
     * Get the all posts in this category.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        $this->attributes['slug'] = str_slug($value);
    }
}
