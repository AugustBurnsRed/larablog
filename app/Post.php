<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $guarded = [];

    /**
     * Get the category of the post.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
    }

    public function setSlugAttribute($value) {
        if (!$value) {
            $this->attributes['slug'] = str_slug($this->title);
        } else {
            $this->attributes['slug'] = str_slug($value);
        }
    }
}
