<?php

namespace App\Http\Controllers;

use Auth;
use Input;
use Redirect;
use Request;
use App\Category;
use App\Post;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            $posts = Post::orderBy('created_at', 'desc')
            ->paginate(config('blog.posts_per_page'));
        	return view('post.index', compact('posts'));
        } else {
            $posts = Post::where([
                ['created_at', '<=', Carbon::now()],
                ['active', 0]
                ]);
            if (Request::get('cat')) {
                $posts = $posts->where('category_id', Request::get('cat'));
            }
            $posts = $posts->orderBy('created_at', 'desc')
                            ->paginate(config('blog.posts_per_page'));
        	return $posts;
        }

    }

    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('post.create', compact('categories'));
    }

    public function store()
    {
        if (!Request::has('created_at')) {
            Request::merge(array('created_at' => Carbon::now()));
        }

        $input = Request::all();
        Post::create($input);
     
        return Redirect::route('post.index')->with('message', 'Post created');
    }

    public function show($id)
    {
        $post = Post::find($id);
        $post->category_name = $post->category->name;
        if (Auth::check()) {
            return view('post.show')->withPost($post);
        }else{
            return $post;
        } 
    }

    public function edit($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $categories = Category::pluck('name', 'id');

        return view('post.edit', compact('post', 'categories'));
    }

    public function update($slug, Request $request)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $post->update(Request::all());
     
        return Redirect::route('post.show', $post->slug)->with('message', 'Post updated.');
    }

    public function destroy($id)
    {
        Post::destroy($id);
     
        return Redirect::route('post.index')->with('message', 'Le post a été supprimé.');
    }
}
