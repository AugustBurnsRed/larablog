<?php
header('Access-Control-Allow-Origin: *');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Route::resource('post', 'PostController');

Route::resource('category', 'CategoryController');

Auth::routes();
