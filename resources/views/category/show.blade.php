@extends('layouts.app')

@section('content')
  <h1>{{ $category->name }}</h1>
	
	<h3>Post dans cette catégorie:</h3>
	<ul>
	  @foreach ($category->posts as $post)
			<li>
        <a href="/post/{{ $post->slug }}">{{ $post->title }}</a>
      </li>
	  @endforeach
  </ul>
  {!! link_to_route('category.index', 'Retour', array(), array('class' => 'btn btn-primary')) !!}
  {!! link_to_route('category.edit', 'Éditer', array($category->slug), array('class' => 'btn btn-primary')) !!}
@endsection