@extends('layouts.app')

@section('content')
  <ul>
    @foreach ($categories as $category)
      <li>
        <a href="/category/{{ $category->slug }}">{{ $category->name }}</a> / 
        {!! link_to_route('category.edit', 'Éditer', array($category->slug)) !!}
      </li>
    @endforeach
  </ul>
  <hr>
  {!! link_to_route('category.create', 'Nouvelle catégorie', array(), array('class' => 'btn btn-primary')) !!}
@endsection