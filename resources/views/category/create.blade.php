@extends('layouts.app')
 
@section('content')
    <h2>Ajouté une nouvelle catégorie</h2>
 
    {!! Form::model(new App\Category, ['route' => ['category.store']]) !!}
        @include('category/partials/_form', ['submit_text' => 'Ajouté catégorie'])
    {!! Form::close() !!}
@endsection