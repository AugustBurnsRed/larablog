@extends('layouts.app')
 
@section('content')
    <h2>Editer la catégory</h2>
 
    {!! Form::model($category, ['method' => 'PATCH', 'route' => ['category.update', $category->slug]]) !!}
        @include('category/partials/_form', ['submit_text' => 'Éditer la catégorie'])
    {!! Form::close() !!}

    {!! Form::open(['method' => 'DELETE',
    	'class' => 'destroy',
		  'route' => ['category.destroy', $category->id],
		  'onsubmit' => 'return ConfirmDelete()']) !!}
		  {!! Form::submit('Supprimer', ['class'=>'btn btn-danger pull-right']) !!}
		{!! Form::close() !!}

		<script>

		  function ConfirmDelete()
		  {
			  return confirm("Êtes-vous sûre de vouloir supprimer cette catégorie??");
		  }

		</script>
@endsection