<div class="form-group">
    {!! Form::text('name',null, array('class'=>'form-control input-md',
    																	 'placeholder'=>'Titre')) 
    !!}
</div>

<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn btn-success pull-left']) !!}
</div>