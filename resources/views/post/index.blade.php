@extends('layouts.app')

@section('content')
  <h1>{{ config('blog.title') }}</h1>
  {!! link_to_route('post.create', 'Create Post', array(), array('class' => 'btn btn-primary pull-right')) !!}
  <h5>Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}</h5>
  <hr>
  <ul>
    @foreach ($posts as $post)
      <li>
        <a href="/post/{{ $post->id }}">{{ $post->title }}</a>
        <span class="bg-info">
          <em>({{ $post->created_at->format('M jS Y g:ia') }})</em>
        </span>
        /
        {!! link_to_route('post.edit', 'Éditer', array($post->slug)) !!}
        <p>
          {{ str_limit($post->content) }}
        </p>
      </li>
    @endforeach
  </ul>
  <hr>
  {{ $posts->links() }}<br>
@endsection