@extends('layouts.app')

@section('content')
  <h1>{{ $post->title }}</h1>
  <p>slug: {{ $post->slug }}</p>
  <h5>{{ $post->created_at->format('M jS Y g:ia') }}</h5>
  <p>actif: {{ $post->active }}</p>
  @if ($post->category)
    <p>catégorie: {{ $post->category->name }}</p>
  @endif
  <hr>
  {!! nl2br(e($post->content)) !!}
  <hr>
  {!! link_to_route('post.index', 'Retour', array(), array('class' => 'btn btn-primary')) !!}
  {!! link_to_route('post.edit', 'Éditer', array($post->slug), array('class' => 'btn btn-primary')) !!}
@endsection