@extends('layouts.app')
 
@section('content')
    <h2>Editer le post</h2>
 
    {!! Form::model($post, ['method' => 'PATCH', 'route' => ['post.update', $post->slug]]) !!}
        @include('post/partials/_form', ['submit_text' => 'Éditer le post'])
    {!! Form::close() !!}

    {!! Form::open(['method' => 'DELETE',
    	'class' => 'destroy',
		  'route' => ['post.destroy', $post->id],
		  'onsubmit' => 'return ConfirmDelete()']) !!}
		  {!! Form::submit('Supprimer', ['class'=>'btn btn-danger pull-right']) !!}
		{!! Form::close() !!}

		<script>

		  function ConfirmDelete()
		  {
			  return confirm("Êtes-vous sûre de vouloir supprimer ce post?");
		  }

		</script>
@endsection