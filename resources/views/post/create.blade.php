@extends('layouts.app')
 
@section('content')
    <h2>Créer nouveau post</h2>
 
    {!! Form::model(new App\Post, ['route' => ['post.store']]) !!}
        @include('post/partials/_form', ['submit_text' => 'Create Post'])
    {!! Form::close() !!}
@endsection