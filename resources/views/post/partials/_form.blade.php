<!-- titre -->
<div class="form-group">
    {!! Form::label('title', 'Titre:') !!}
    {!! Form::text('title',null, array('class'=>'form-control input-md')) 
    !!}
</div>
<!-- slug -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug',null, array('class'=>'form-control input-md',
                                       'placeholder'=>'laisser vide pour en avoir un automatiquement')) 
    !!}
</div>
<!-- actif ou non -->
<div class="form-group">
	{!! Form::label('active', 'Actif:') !!}
	{!! Form::hidden('active', 0) !!} 
  {!! Form::checkbox('active', true) !!} 
</div>
<!-- date de création -->
<div class="form-group">
  {!! Form::label('active', 'Date de mise en ligne:') !!}
  {!! Form::text('created_at',null, array('class'=>'form-control input-md',
                                       'placeholder'=>'ex: 2016-12-25 15:30:00')) 
    !!}
</div>
<!-- catégorie -->
<div class="form-group">
	{!! Form::label('category', 'Catégorie:') !!}
  {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
</div>
<!-- description -->
<div class="form-group">
    {!! Form::label('content', 'Description:') !!}
    {!! Form::textarea('content',null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn btn-success pull-left']) !!}
</div>